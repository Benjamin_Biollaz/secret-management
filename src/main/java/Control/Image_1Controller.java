package Control;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

import Model.Image_1;
import View.View_1;

public class Image_1Controller {

    private Image_1 image;
    private View_1 view;

    public Image_1Controller(View_1 view, Image_1 image) {
        this.image = image;
        this.view = view;
        viewInitialize();
    }

    private void viewInitialize() {
        image.setIcon(resizeWithRatio(image.getIcon(), 400));
        view.getLabel().setIcon(image.getIcon());
        view.setLabel(new JLabel());
    }

    public void initialize() {
        view.getSlider().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int value = view.getSlider().getValue();
                image.setIcon(resizeWithRatio(image.getIcon(), 200 * value / 5));
                // trying to refresh...
                view.setLabel(new JLabel());
                view.getLabel().setIcon(image.getIcon());
                view.getLabel().updateUI();
            }
        });
    }

    public ImageIcon resizeWithRatio(ImageIcon imageIcon, int dimension) {

        double imageHeight = imageIcon.getIconHeight();
        double imageWidth = imageIcon.getIconWidth();
        double ratio = imageHeight / imageWidth;

        if (ratio < 1) {
            imageIcon = resize(imageIcon, dimension, (int) (dimension * ratio));
        } else {
            imageIcon = resize(imageIcon, ((int) (dimension * ratio)), dimension);
        }
        return imageIcon;
    }

    public ImageIcon resize(ImageIcon imageIcon, int width, int height) {

        // transforms it
        Image image = imageIcon.getImage();
        // scales it the smooth way
        Image newImg = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        // transforms it back
        imageIcon = new ImageIcon(newImg);
        return imageIcon;
    }
}
