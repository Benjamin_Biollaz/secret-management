package Control;
import View.View;
import Model.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
    private Person person;
    private View view;

    public Controller(Person person, View view) {
        this.person = person;
        this.view = view;
        viewInitialize();
    }

    private void viewInitialize(){
        view.getFieldFirstName().setText(person.getFirstName());
        view.getFieldLastName().setText(person.getLastName());
    }

    public void initialize(){
        view.getButtonSave().addActionListener(e -> save());
        view.getButtonHello().addActionListener(e-> sayHello());
    }

    private void sayHello(){
        JOptionPane.showMessageDialog(null, "Hello, "+person.getFirstName()+" "+person.getLastName());
    }

    private void save(){
        person.setFirstName(view.getFieldFirstName().getText());
        person.setLastName(view.getFieldLastName().getText());
    }
}
