package Control;

import Model.Integer_1;
import View.View_1;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Integer_1Controller {
    private View_1 view;
    private Integer_1 integer;

    public Integer_1Controller(View_1 view, Integer_1 integer) {
        this.view = view;
        this.integer = integer;
    }

    private void viewInitialize() {
        view.getLabel().setText(String.valueOf(integer.getInteger()));
    }

    public void initialize(){
        view.getSlider().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int n = view.getSlider().getValue();
                System.out.println(n);
                integer.setInteger(n);
                view.getLabel().setText(String.valueOf(integer.getInteger()));
            }
        });

    }
}
