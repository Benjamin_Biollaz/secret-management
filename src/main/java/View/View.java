package View;

import javax.swing.*;
import java.awt.*;

public class View {
    private final int WIDTH = 640;
    private final int HEIGHT = 480;
    private JFrame frame;
    private JLabel firstName;
    private JLabel lastName;
    private JTextField fieldFirstName;
    private JTextField fieldLastName;
    private JButton buttonSave;
    private JButton buttonHello;

    public View(String titre) {
        // Creation of UI objects
        frame = new JFrame(titre);
        firstName = new JLabel("first name:");
        lastName = new JLabel("last name:");
        fieldFirstName = new JTextField(10);
        fieldLastName = new JTextField(10);
        buttonSave = new JButton("Save");
        buttonHello = new JButton("Hello");

        // Add UI objects
        frame.add(firstName);
        frame.add(fieldFirstName);
        frame.add(lastName);
        frame.add(fieldLastName);
        frame.add(buttonSave);
        frame.add(buttonHello);

        // Window initialization
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(WIDTH, HEIGHT);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new FlowLayout());
        frame.setVisible(true);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JLabel getFirstName() {
        return firstName;
    }

    public void setFirstName(JLabel firstName) {
        this.firstName = firstName;
    }

    public JLabel getLastName() {
        return lastName;
    }

    public void setLastName(JLabel lastName) {
        this.lastName = lastName;
    }

    public JTextField getFieldFirstName() {
        return fieldFirstName;
    }

    public void setFieldFirstName(JTextField fieldFirstName) {
        this.fieldFirstName = fieldFirstName;
    }

    public JTextField getFieldLastName() {
        return fieldLastName;
    }

    public void setFieldLastName(JTextField fieldLastName) {
        this.fieldLastName = fieldLastName;
    }

    public JButton getButtonSave() {
        return buttonSave;
    }

    public void setButtonSave(JButton buttonSave) {
        this.buttonSave = buttonSave;
    }

    public JButton getButtonHello() {
        return buttonHello;
    }

    public void setButtonHello(JButton buttonHello) {
        this.buttonHello = buttonHello;
    }
}
