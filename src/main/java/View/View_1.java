package View;

import Model.Image_1;

import javax.swing.*;
import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;

public class View_1 {
    public final int WIDTH = 1200;
    public final int HEIGHT = 600;
    private JFrame frame;
    private JSlider slider;
    private JLabel label;


    public View_1() throws MalformedURLException {
        // Creation of UI objects
        frame = new JFrame("Today's exam");
        slider = new JSlider(1,1,10,5);
        slider.setMajorTickSpacing(1);
        slider.setPaintTicks(true);
        label =  new JLabel();
        Hashtable labelTable = new Hashtable();
        labelTable.put(1, new JLabel("1") );
        labelTable.put(5, new JLabel("5") );
        labelTable.put(10, new JLabel("10") );
        slider.setLabelTable(labelTable);
        slider.setPaintLabels(true);


        // Add UI objects
        frame.add(slider);
        frame.add(label);

        // Window initialization
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(WIDTH, HEIGHT);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new FlowLayout());
        frame.setVisible(true);
    }

    public JSlider getSlider() {
        return slider;
    }

    public void setSlider(JSlider slider) {
        this.slider = slider;
    }

    public JLabel getLabel() {
        return label;
    }

    public void setLabel(JLabel label) {
        frame.remove(this.label);
        this.label = label;
        frame.add(label);
        label.updateUI();
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }
}
