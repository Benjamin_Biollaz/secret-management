
import Control.Image_1Controller;
import Model.Image_1;
import Model.Integer_1;
import View.View;
import View.View_1;

import javax.swing.*;
import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;

public class Main {
    public static void main(String[] args) throws MalformedURLException {
        View_1 view = new View_1();
        Image_1 image = new Image_1(new ImageIcon(new URL("https://www.google.com/url?sa=i&url=https%3A%2F%2Ffr.pixers.ch%2Fposters%2Fblue-orchid-32832853&psig=AOvVaw0QXvsaIfiv8Z3PXp-WV6ne&ust=1651052046460000&source=images&cd=vfe&ved=0CAwQjRxqFwoTCJCA4ai2sfcCFQAAAAAdAAAAABAD")));
        Image_1Controller imc = new Image_1Controller(view, image);
        imc.initialize();
    }

}
