package Model;

import javax.swing.*;
import java.net.MalformedURLException;
import java.net.URL;

public class Image_1 {

    private ImageIcon icon;

    public Image_1(ImageIcon icon) throws MalformedURLException {
        this.icon = icon;
    }

    public ImageIcon getIcon() {
        return icon;
    }

    public void setIcon(ImageIcon icon) {
        this.icon = icon;
    }
}
